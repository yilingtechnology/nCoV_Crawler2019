package fun.ticsmyc.main;


import fun.ticsmyc.email.EmailUtil;
import fun.ticsmyc.service.InformationService;
import org.apache.commons.io.FileUtils;

import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * 用于可执行jar包。。。
 * @author Ticsmyc
 * @package fun.ticsmyc.main
 * @date 2020-01-26 21:51
 */
public class SendEmail {
    public static void main(String[] args) {
        //每十分钟执行一次
//        new Timer("testTimer").schedule(new TimerTask() {
//            @Override
//            public void run() {
//                fffffuck();
//            }
//        }, 1000,600000);

        String subject="最新动态";
        String content="月缺月圆越相思";

        //读取收件人列表
        Properties properties = null;
        List<String> toEmailList = null;
        ClassLoader classLoader = SendEmail.class.getClassLoader();
        if (classLoader != null) {
            URL toEmailFile = classLoader.getResource("emailReceiver.properties");
            properties = new Properties();
            try {
                properties.load(classLoader.getResource("email.properties").openStream());
            } catch (IOException e) {
             }

            try {
                toEmailList = FileUtils.readLines(new File(URLDecoder.decode(toEmailFile.getPath(), "utf-8")));
            } catch (IOException e) {
             }
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for (String toUserEmail : toEmailList) {
            if (!toUserEmail.equals("")) {
                try {
                    EmailUtil.sendEmail((String) properties.get("email.authCode"), (String) properties.get("email.fromEmail"), toUserEmail, dateFormat.format(new Date()) + subject, content);
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void fffffuck(){
        InformationService informationService = new InformationService();
        informationService.getNews();
    }
}
